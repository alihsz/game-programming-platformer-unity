using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void onPlayButtonClick()
    {
        Debug.Log("Play button clicked");
        SceneManager.LoadScene("Level1");
    }

    public void onExitButtonClick()
    {
        Debug.Log("Exit button clicked");
        Application.Quit();
    }
}
