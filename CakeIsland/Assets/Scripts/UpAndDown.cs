using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpAndDown : MonoBehaviour
{
    private int direction = 1;
    public double topPosition;
    public double bottomPosition;
    public float moveSpeed;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localPosition.y > topPosition)
        {
            direction = -1;
        }

        if (transform.localPosition.y < bottomPosition) {
            direction = 1;
        }

        transform.Translate(Vector3.up * direction * moveSpeed * Time.deltaTime);
    }
}
