using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowScoreWinScreen : MonoBehaviour
{

    public TMP_Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score: " + PlayerControllerLevel2.score;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
