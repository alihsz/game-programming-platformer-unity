using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // The falling boulder for the obstacle 
    public GameObject fallingBoulder;
    // The variable for when all collectibles collected, make secret key appear
    public GameObject cheeseKey;

    // Creating audio source array for different sounds
    public AudioSource[] audioSource;

    public TMP_Text hudText; 

    // variable for collecting the collectibles.
    private int cookiesCollected = 0;
    private int candyCanesCollected = 0;
    private int oreosCollected = 0;
    private int cookiesGoal = 10;
    private int candyCanesGoal = 10;
    private int oreosGoal = 10;

    public static int score = 0;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Making sure that start method started...");
        score = 0;
        audioSource = GetComponents<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("CookieCollectible"))
        {
            collision.gameObject.SetActive(false);
            audioSource[2].Play();
            cookiesCollected += 1;
            score += 5;
            hudText.text = "Level: 1\n" +
                "Cookies: " + cookiesCollected + "/" + cookiesGoal + "\n" +
                "Oreos: " + oreosCollected + "/" + oreosGoal + "\n" +
                "Candy Canes: " + candyCanesCollected + "/" + candyCanesGoal + "\n\n" + 
                "Score: " + score;
            if (cookiesCollected == cookiesGoal && oreosCollected == oreosGoal && candyCanesCollected == candyCanesGoal)
            {
                Instantiate(cheeseKey, new Vector3(358.156372f, 42.1010017f, -104.735504f), Quaternion.identity);
                hudText.text = "Go to the top of the castle to find the secret key!\n\nScore: " + score;
            }
        }
        if (collision.gameObject.tag.Equals("OreoCollectible"))
        {
            collision.gameObject.SetActive(false);
            audioSource[3].Play();
            oreosCollected += 1;
            score += 10;
            hudText.text = "Level: 1\n" +
                "Cookies: " + cookiesCollected + "/" + cookiesGoal + "\n" +
                "Oreos: " + oreosCollected + "/" + oreosGoal + "\n" +
                "Candy Canes: " + candyCanesCollected + "/" + candyCanesGoal + "\n\n" + 
                "Score: " + score;
            if (cookiesCollected == cookiesGoal && oreosCollected == oreosGoal && candyCanesCollected == candyCanesGoal)
            {
                Instantiate(cheeseKey, new Vector3(358.156372f, 42.1010017f, -104.735504f), Quaternion.identity);
                hudText.text = "Go to the top of the castle to find the secret key!\n\nScore: " + score;
            }
        }
        if (collision.gameObject.tag.Equals("CandyCaneCollectible"))
        {
            collision.gameObject.SetActive(false);
            audioSource[1].Play();
            candyCanesCollected += 1;
            score += 15;
            hudText.text = "Level: 1\n" +
                "Cookies: " + cookiesCollected + "/" + cookiesGoal + "\n" +
                "Oreos: " + oreosCollected + "/" + oreosGoal + "\n" +
                "Candy Canes: " + candyCanesCollected + "/" + candyCanesGoal + "\n\n" + 
                "Score: " + score;
            if (cookiesCollected == cookiesGoal && oreosCollected == oreosGoal && candyCanesCollected == candyCanesGoal)
            {
                Instantiate(cheeseKey, new Vector3(358.156372f, 42.1010017f, -104.735504f), Quaternion.identity);
                hudText.text = "Go to the top of the castle to find the secret key!\n\nScore: " + score;
            }
        }

        if (collision.gameObject.tag.Equals("Key")) {
            audioSource[0].Play();
            SceneManager.LoadScene("Level2");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Trigger"))
        {
            fallingBoulder.GetComponent<Rigidbody>().useGravity = true;
        }
    }

}
