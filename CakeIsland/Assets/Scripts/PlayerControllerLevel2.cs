using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerControllerLevel2 : MonoBehaviour
{
    // The falling boulder for the obstacle 
    public GameObject fallingBoulder;
    // The variable for when all collectibles collected, make secret key appea

    // Creating audio source array for different sounds
    public AudioSource[] audioSource;

    public TMP_Text hudText;

    public static int score = PlayerController.score;

    // variable for collecting the collectibles.
    private int cookiesCollected = 0;
    private int candyCanesCollected = 0;
    private int oreosCollected = 0;
    private int cookiesGoal = 15;
    private int candyCanesGoal = 15;
    private int oreosGoal = 15;

    // Start is called before the first frame update
    void Start()
    {
        score = PlayerController.score;
        Debug.Log("Starting PlayerControllerLevel2...");
        audioSource = GetComponents<AudioSource>();
        hudText.text = "Level: 2\n" +
                "Cookies: " + cookiesCollected + "/" + cookiesGoal + "\n" +
                "Oreos: " + oreosCollected + "/" + oreosGoal + "\n" +
                "Candy Canes: " + candyCanesCollected + "/" + candyCanesGoal + "\n\n" + 
                "Score: " + score;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision) 
    {
        if (collision.gameObject.tag.Equals("CookieCollectible")) {
            collision.gameObject.SetActive(false);
            audioSource[2].Play();
            cookiesCollected += 1;
            score += 5; 
            hudText.text = "Level: 2\n" +
                "Cookies: " + cookiesCollected + "/" + cookiesGoal + "\n" +
                "Oreos: " + oreosCollected + "/" + oreosGoal + "\n" +
                "Candy Canes: " + candyCanesCollected + "/" + candyCanesGoal + "\n\n" + 
                "Score: " + score;
            if (cookiesCollected == cookiesGoal && oreosCollected == oreosGoal && candyCanesCollected == candyCanesGoal) {
                SceneManager.LoadScene("Win");
            }
        }
        if (collision.gameObject.tag.Equals("OreoCollectible")) {
            collision.gameObject.SetActive(false);
            audioSource[3].Play();
            oreosCollected += 1;
            score += 10;
            hudText.text = "Level: 2\n" +
                "Cookies: " + cookiesCollected + "/" + cookiesGoal + "\n" +
                "Oreos: " + oreosCollected + "/" + oreosGoal + "\n" +
                "Candy Canes: " + candyCanesCollected + "/" + candyCanesGoal + "\n\n" + 
                "Score: " + score;
            if (cookiesCollected == cookiesGoal && oreosCollected == oreosGoal && candyCanesCollected == candyCanesGoal) {
                SceneManager.LoadScene("Win");
            }
        }
        if (collision.gameObject.tag.Equals("CandyCaneCollectible")) {
            collision.gameObject.SetActive(false);
            audioSource[1].Play();
            candyCanesCollected += 1;
            score += 15;
            hudText.text = "Level: 2\n" +
                "Cookies: " + cookiesCollected + "/" + cookiesGoal + "\n" +
                "Oreos: " + oreosCollected + "/" + oreosGoal + "\n" +
                "Candy Canes: " + candyCanesCollected + "/" + candyCanesGoal + "\n\n" +
                "Score: " + score;
            if (cookiesCollected == cookiesGoal && oreosCollected == oreosGoal && candyCanesCollected == candyCanesGoal) {
                SceneManager.LoadScene("Win");
            }
        }
    }

     private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Trigger"))
        {
            fallingBoulder.GetComponent<Rigidbody>().useGravity = true;
        }
    }
}
