using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftToRight : MonoBehaviour
{
    private int direction = 1;
    public double leftPostion;
    public double rightPosition;
    public int moveSpeed;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localPosition.z > leftPostion)
        {
            direction = -1;
        }

        if (transform.localPosition.z < rightPosition) {
            direction = 1;
        }

        transform.Translate(Vector3.forward * direction * moveSpeed * Time.deltaTime);
    }
}
