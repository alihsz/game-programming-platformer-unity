using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleUpAndDown : MonoBehaviour
{
    private double position = 0;
    private int direction = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        position += 0.1 * direction;
        if (position > 30)
        {
            direction = -1;
        }
        if (position < 2)
        {
            direction = 1;
        }

        transform.Translate(Vector3.up * direction * Time.deltaTime);
    }
}
