using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockCursorScript : MonoBehaviour
{
    // Start is called before the first frame update
    [System.Obsolete]
    void Start()
    {
        Cursor.visible = true;
        Screen.lockCursor = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
